##
## Copyright Frank Drop and Mario Olivari.
##
## This file is part of Offline Motion Simulation Framework (OMSF) fork.
## (see https://gitlab.com/predictive-cueing/omsf-fork).
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Lesser General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Lesser General Public License for more details.
##
## You should have received a copy of the GNU Lesser General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##

from typing import Any, List, Union

import casadi as cs  # type: ignore
import numpy as np

import casadi_extras as ct
from omsf import typing

from .dynamic_system import DynamicSystem
from .gravity import DEFAULT_GRAVITY
from .motion_limits import MotionLimits
from .typing import T_NDArray


class FunctionsForDoubleIntegrator(object):
    def __init__(
        self,
        q: cs.MX,
        y: Union[cs.MX, ct.struct_MX],
        transformation_matrix_platform_world: Union[cs.MX, typing.T_NDArray],
        head_pva: ct.struct_MX,
        head_inertial: ct.struct_MX,
        v: Union[cs.MX, None] = None,
        u: Union[cs.MX, None] = None,
        transformation_matrix_head_platform: Union[cs.MX, None] = None,
        cg: Union[cs.MX, typing.T_NDArray, None] = None,
    ) -> None:
        self._q = q
        self._y = y
        self._transformation_matrix_PW = transformation_matrix_platform_world
        self._cg = cg
        self._head_pva = head_pva
        self._head_inertial = head_inertial

        # Initialize v, u, T_HP. Default values if None. This can happen if y or constraints are not function of them.
        if v is None:
            self._v: cs.MX = cs.MX.sym("v", q.shape)  # type: ignore
        else:
            assert v.shape == q.shape  # type: ignore
            self._v: cs.MX = v

        if u is None:
            self._u: cs.MX = cs.MX.sym("u", q.shape)  # type: ignore
        else:
            assert u.shape == q.shape  # type: ignore
            self._u: cs.MX = u

        if transformation_matrix_head_platform is None:
            self._transformation_matrix_HP: cs.MX = cs.MX.sym("T_HP", 4, 4)  # type: ignore
        else:
            assert transformation_matrix_head_platform.shape == (4, 4)  # type: ignore
            self._transformation_matrix_HP: cs.MX = transformation_matrix_head_platform

    @property
    def axesPosition(self) -> cs.MX:
        return self._q

    @property
    def axesVelocity(self) -> cs.MX:
        return self._v

    @property
    def axesAcceleration(self) -> cs.MX:
        return self._u

    @property
    def platformToWord(self) -> Union[cs.MX, typing.T_NDArray]:
        return self._transformation_matrix_PW

    @property
    def headToPlatform(self) -> cs.MX:
        return self._transformation_matrix_HP

    @property
    def outputExpression(self) -> Union[cs.MX, ct.struct_MX]:
        return self._y

    @property
    def constraintExpression(self) -> Union[cs.MX, typing.T_NDArray, None]:
        return self._cg

    @property
    def headPva(self) -> ct.struct_MX:
        return self._head_pva

    @property
    def headInertial(self) -> ct.struct_MX:
        return self._head_inertial


class DoubleIntegratorDynamicSystem(DynamicSystem):
    def __init__(
        self,
        double_integrator_functions: FunctionsForDoubleIntegrator,
        motion_limits: List[MotionLimits],
        nominal_position: typing.T_NDArray,
        constraint_lower_limit: Union[List[float], T_NDArray, None] = None,
        constraint_upper_limit: Union[List[float], T_NDArray, None] = None,
        param: ct.BoundedVariable = ct.BoundedVariable(),  # type: ignore
        gravity: typing.T_NDArray = DEFAULT_GRAVITY,
        head_to_platform: typing.T_NDArray = np.identity(4),
        **kwargs: "dict[str, Any]"
    ):
        """Constructor"""
        # Init state
        x: ct.struct_MX = ct.struct_MX([ct.entry("q", expr=double_integrator_functions.axesPosition), ct.entry("v", expr=double_integrator_functions.axesVelocity)])  # type: ignore

        # Init DAE function
        x_dot = ct.struct_symMX(x)
        dae: cs.MX = cs.vertcat(x_dot["q"] - double_integrator_functions.axesVelocity, x_dot["v"] - double_integrator_functions.axesAcceleration)  # type: ignore
        assert isinstance(dae, cs.MX)

        # Axes limits
        q_min = np.array([l.positionMin for l in motion_limits])
        q_max = np.array([l.positionMax for l in motion_limits])
        v_min = np.array([l.velocityMin for l in motion_limits])
        v_max = np.array([l.velocityMax for l in motion_limits])
        u_min = np.array([l.accelerationMin for l in motion_limits])
        u_max = np.array([l.accelerationMax for l in motion_limits])

        # Inequality constraints
        if double_integrator_functions.constraintExpression is None:
            constraint: ct.Inequality = ct.Inequality()  # type: ignore
        else:
            Nc: int = double_integrator_functions.constraintExpression.shape[0]  # type: ignore
            if constraint_lower_limit is None or constraint_upper_limit is None:
                raise RuntimeError("Constraint limits must be set when constraints are defined.")
            if len(constraint_lower_limit) != Nc:
                raise RuntimeError("The shape of the constraint lower limit must be equal to the number of constraints functions.")
            if len(constraint_upper_limit) != Nc:
                raise RuntimeError("The shape of the constraint lower limit must be equal to the number of constraints functions.")

            constr = ct.struct_MX([ct.entry("constr", expr=double_integrator_functions.constraintExpression)])
            lbg = constr()
            ubg = constr()
            lbg["constr"] = constraint_lower_limit
            ubg["constr"] = constraint_upper_limit

            constraint: ct.Inequality = ct.Inequality(expr=constr, lb=lbg, ub=ubg)

        # Init base class
        DynamicSystem.__init__(
            self,
            state_derivative=x_dot,
            state=ct.Inequality(
                expr=x,
                lb=x({"q": q_min, "v": v_min}),  # type:ignore
                ub=x({"q": q_max, "v": v_max}),  # type:ignore
                nominal=x({"q": nominal_position, "v": 0}),  # type:ignore
            ),
            input=ct.Inequality(
                expr=double_integrator_functions.axesAcceleration,
                lb=u_min,
                ub=u_max,
                nominal=cs.DM.zeros(double_integrator_functions.axesAcceleration.numel()),  # type:ignore
            ),
            dae=dae,
            output_expr=double_integrator_functions.outputExpression,
            platform_to_world=double_integrator_functions.platformToWord,
            constraint=constraint,
            param=param,
            gravity_default=gravity,
            head_to_platform_default=head_to_platform,
            head_to_platform_symbolic=double_integrator_functions.headToPlatform,
            head_pva_expr=double_integrator_functions.headPva,
            head_inertial_expr=double_integrator_functions.headInertial,
        )

        self._axesLimits = motion_limits

    @property
    def numberOfAxes(self):
        return len(self._axesLimits)

    @staticmethod
    def _makeVariables(n_axes: int):
        x = ct.struct_symMX([ct.entry("q", shape=n_axes), ct.entry("v", shape=n_axes)])  # type:ignore

        z = ct.struct_symMX([ct.entry("empty", shape=0)])  # type:ignore

        u = ct.struct_symMX([ct.entry("u", shape=n_axes)])  # type:ignore

        return x, z, u
