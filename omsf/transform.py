##
## Copyright (c) 2015-2018 Mikhail Katliar, Max Planck Institute for Biological Cybernetics.
##
## This file is part of Offline Motion Simulation Framework (OMSF)
## (see https://github.com/mkatliar/omsf).
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Lesser General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Lesser General Public License for more details.
##
## You should have received a copy of the GNU Lesser General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##
"""
Created on Oct 9, 2015

@author: mkatliar
"""
import math
import typing
from typing import List, Union, overload

import numpy as np
import numpy.typing as npt

import casadi as cs  # type:ignore
from casadi.casadi import DM, MX  # type:ignore

from .typing import T_NDArray


@overload
def mul(input1: float, input2: float) -> float: ...


@overload
def mul(input1: T_NDArray, input2: T_NDArray) -> T_NDArray: ...


@overload
def mul(input1: float, input2: T_NDArray) -> T_NDArray: ...


@overload
def mul(input1: T_NDArray, input2: float) -> T_NDArray: ...


@overload
def mul(input1: MX, input2: T_NDArray) -> MX: ...


@overload
def mul(input1: T_NDArray, input2: MX) -> MX: ...


@overload
def mul(input1: MX, input2: MX) -> MX: ...


@overload
def mul(input1: float, input2: MX) -> MX: ...


@overload
def mul(input1: MX, input2: float) -> MX: ...


@overload
def mul(input1: DM, input2: DM) -> DM: ...


def mul(input1: Union[float, MX, DM, T_NDArray], input2: Union[float, MX, DM, T_NDArray]) -> Union[float, MX, DM, T_NDArray]:
    return cs.mtimes(input1, input2)  # type: ignore # _mul(input1, input2)


def inv(T: MX) -> MX:
    """Inverse of the transform"""

    # Not using cs.inv(), so that no linear solver is involved => expand() works.
    invR: MX = cs.transpose(getRotationMatrix(T))  # type: ignore
    # return cs.vertcat(
    #    cs.horzcat(invR, cs.mtimes(invR, getTranslationVector(T))),
    #    cs.horzcat(cs.MX.zeros(1, 3), 1)
    # )
    return cs.vertcat(cs.horzcat(invR, -cs.mtimes(invR, getTranslationVector(T))), cs.horzcat(0, 0, 0, 1))  # type: ignore


@overload
def identityTyped(dtype: float) -> T_NDArray: ...


@overload
def identityTyped(dtype: MX) -> MX: ...


def identityTyped(dtype: Union[float, MX]) -> Union[MX, T_NDArray]:
    """Identity transform"""

    if _isSymbolicVar(dtype):
        return cs.MX.eye(4)  # type: ignore
    else:
        return np.identity(4)


def identity(dtype: type = float) -> Union[MX, T_NDArray]:
    """Identity transform"""

    if _isSymbolic(dtype):
        return cs.MX.eye(4)  # type: ignore
    else:
        return np.identity(4)


@overload
def translation(xyz: List[float]) -> T_NDArray: ...


@overload
def translation(xyz: T_NDArray) -> T_NDArray: ...


@overload
def translation(xyz: MX) -> MX: ...


def translation(xyz: Union[List[float], MX, T_NDArray]) -> Union[MX, T_NDArray]:
    """Translation by vector"""

    T = eye(xyz[0])  # type:ignore
    T[:3, 3] = xyz
    return T


@overload
def translationX(x: MX) -> MX: ...


@overload
def translationX(x: float) -> npt.NDArray[np.float64]: ...


@overload
def translationX(x: npt.NDArray[np.float64]) -> npt.NDArray[np.float64]: ...


def translationX(x: Union[MX, float, npt.NDArray[np.float64]]) -> Union[MX, npt.NDArray[np.float64]]:
    """Translation along the X axis"""

    T = eye(x)
    T[0, 3] = x
    return T


@overload
def translationY(y: MX) -> MX: ...


@overload
def translationY(y: npt.NDArray[np.float64]) -> npt.NDArray[np.float64]: ...


def translationY(y: Union[MX, npt.NDArray[np.float64]]) -> Union[MX, npt.NDArray[np.float64]]:
    """Translation along the Y axis"""

    T = eye(y)
    T[1, 3] = y
    return T


@overload
def translationZ(z: MX) -> MX: ...


@overload
def translationZ(z: npt.NDArray[np.float64]) -> npt.NDArray[np.float64]: ...


@overload
def translationZ(z: float) -> npt.NDArray[np.float64]: ...


def translationZ(z: Union[MX, float, npt.NDArray[np.float64]]) -> Union[MX, npt.NDArray[np.float64]]:
    """Translation along the Z axis"""

    T = eye(z)
    T[2, 3] = z
    return T


@overload
def rotationZ(theta: MX) -> MX: ...


@overload
def rotationZ(theta: float) -> npt.NDArray[np.float64]: ...


def rotationZ(theta: Union[MX, float]) -> Union[MX, npt.NDArray[np.float64]]:
    """Rotation around the Z axis"""

    T = eye(theta)
    T[0, 0] = T[1, 1] = cos(theta)
    T[1, 0] = sin(theta)
    T[0, 1] = -T[1, 0]
    return T


@overload
def rotationY(theta: MX) -> MX: ...


@overload
def rotationY(theta: float) -> npt.NDArray[np.float64]: ...


def rotationY(theta: Union[MX, float]) -> Union[MX, npt.NDArray[np.float64]]:
    """Rotation around the Y axis"""

    T = eye(theta)
    T[0, 0] = T[2, 2] = cos(theta)
    T[0, 2] = sin(theta)
    T[2, 0] = -T[0, 2]
    return T


@overload
def rotationX(theta: MX) -> MX: ...


@overload
def rotationX(theta: float) -> npt.NDArray[np.float64]: ...


def rotationX(theta: Union[MX, float]) -> Union[MX, npt.NDArray[np.float64]]:
    """Rotation around the X axis"""

    T = eye(theta)
    T[1, 1] = T[2, 2] = cos(theta)
    T[2, 1] = sin(theta)
    T[1, 2] = -T[2, 1]
    return T


def denavitHartenberg(a: Union[float, cs.MX], d: Union[float, cs.MX], alpha: Union[float, cs.MX], theta: Union[float, cs.MX]) -> MX:
    """Transform between two frames defined by Denavit-Hartenberg parameters"""

    return cs.mtimes([rotationZ(theta), translationZ(d), rotationX(alpha), translationX(a)])  # type: ignore


def getRotationMatrix(T: MX) -> MX:
    """Get rotation matrix form a 4x4 homogeneous transformation matrix."""

    return T[:3, :3]  # type: ignore


def getTranslationVector(T: MX) -> MX:
    """Get rotation matrix form a 4x4 homogeneous transformation matrix."""

    return T[:3, 3]  # type: ignore


_tolCos = 2.0 * math.cos(math.pi / 2.0)
_tolSin = 2.0 * math.sin(math.pi)


@overload
def eye(typedValue: Union[T_NDArray, float]) -> T_NDArray: ...


@overload
def eye(typedValue: MX) -> MX: ...


def eye(typedValue: Union[MX, T_NDArray, float]) -> Union[MX, T_NDArray]:
    if _isSymbolicVar(typedValue):
        return cs.MX.eye(4)  # type: ignore
    else:
        return np.identity(4)


# def _mul(*args):
#     # type: (*ndarray) -> DM
#     return cs.mtimes([*args])


@overload
def cos(x: Union[MX, cs.SX]) -> MX: ...


@overload
def cos(x: float) -> float: ...


def cos(x: Union[MX, cs.SX, float]) -> Union[MX, float]:
    if _isSymbolic(type(x)):
        return cs.cos(x)  # type: ignore
    else:
        c = math.cos(x)
        if math.fabs(c) <= _tolCos:
            return 0.0

        return c


@overload
def sin(x: Union[MX, cs.SX]) -> MX: ...


@overload
def sin(x: float) -> float: ...


def sin(x: Union[MX, cs.SX, float]) -> Union[MX, float]:
    if _isSymbolicVar(x):
        return cs.sin(x)  # type: ignore
    else:
        s = math.sin(x)
        if math.fabs(s) <= _tolSin:
            return 0.0

        return s


def _isSymbolic(t: type) -> bool:
    return t == cs.SX or t == cs.MX


def _isSymbolicVar(t: typing.Any) -> bool:
    return type(t) == cs.SX or type(t) == cs.MX  # type: ignore
