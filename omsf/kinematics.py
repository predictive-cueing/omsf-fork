##
## Copyright (c) 2015-2021 Mikhail Katliar,
## Max Planck Institute for Biological Cybernetics & University of Freiburg.
##
## This file is part of Offline Motion Simulation Framework (OMSF)
## (see https://github.com/mkatliar/omsf).
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Lesser General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Lesser General Public License for more details.
##
## You should have received a copy of the GNU Lesser General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##
# -*- coding: utf-8 -*-
"""
@author: mkatliar
"""

import casadi as cs  # type: ignore
from omsf import transform


class PrismaticLink:
    """Prismatic link class."""

    def __init__(self, a: float, alpha: float, theta: float) -> None:
        self._a = a
        self._alpha = alpha
        self._theta = theta

    def LocalToBase(self, d: float):
        return transform.denavitHartenberg(self._a, d, self._alpha, self._theta)


class RevoluteLink:
    """Revolute link class."""

    def __init__(self, a: float, d: float, alpha: float, theta_scale: float = 1.0, theta_offset: float = 0.0) -> None:
        self._a = a
        self._d = d
        self._alpha = alpha
        self._thetaScale = theta_scale
        self._thetaOffset = theta_offset
        self._TzRxTx: cs.MX = cs.mtimes(  # type: ignore
            [transform.translationZ(self._d), transform.rotationX(self._alpha), transform.translationX(self._a)]
        )

    def LocalToBase(self, theta: float):
        return transform.denavitHartenberg(self._a, self._d, self._alpha, self._thetaScale * theta + self._thetaOffset)
