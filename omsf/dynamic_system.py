##
## Copyright Frank Drop and Mario Olivari.
##
## This file is part of Offline Motion Simulation Framework (OMSF) fork.
## (see https://gitlab.com/predictive-cueing/omsf-fork).
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Lesser General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Lesser General Public License for more details.
##
## You should have received a copy of the GNU Lesser General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##

from typing import Union

import casadi as cs  # type: ignore
import numpy as np

import casadi_extras as ct

from .gravity import DEFAULT_GRAVITY, GRAVITY
from .typing import T_NDArray


class DynamicSystem(object):
    """Defines dynamics and output quantities of a generic dynamic system."""

    def __init__(
        self,
        state_derivative: ct.struct_symMX,
        state: ct.Inequality,
        input: ct.Inequality,
        dae: Union[cs.MX, ct.struct_symMX],
        output_expr: Union[cs.MX, ct.struct_symMX, ct.struct_MX],
        platform_to_world: Union[cs.MX, T_NDArray],
        head_pva_expr: Union[cs.MX, ct.struct_symMX, ct.struct_MX],
        head_inertial_expr: Union[cs.MX, ct.struct_symMX, ct.struct_MX],
        alg_state: ct.BoundedVariable = ct.BoundedVariable(),  # type: ignore
        param: ct.BoundedVariable = ct.BoundedVariable(),  # type: ignore
        constraint: ct.Inequality = ct.Inequality(),  # type: ignore
        gravity_default: T_NDArray = DEFAULT_GRAVITY,
        head_to_platform_default: Union[cs.MX, T_NDArray] = np.identity(4),
        head_to_platform_symbolic: cs.MX = cs.MX.sym("T_HP", 4, 4),  # type: ignore
    ) -> None:
        """Constructor"""

        self.stateDerivative = state_derivative
        self.state = state
        self.algState = alg_state
        self.input = input

        # Parameters
        self.param = param

        # Implicit DAE equations
        self.dae = dae

        # Constraints
        self.constraint = constraint

        # Platform to World expression
        self.T_PW = platform_to_world

        # Gravity
        self.gravity = gravity_default

        # Head to platform transformation
        self.headToPlatform = head_to_platform_default

        # TODO: Check output to be a struct

        # Symbolic and nominal value for output
        self.symbolicOutput = ct.struct_symMX(output_expr)  # type:ignore

        # Symbolic value for reference output
        self.symbolicReferenceOutput = ct.struct_symMX(self.symbolicOutput)

        # Output Function
        self.output = cs.Function(
            "DynamicSystemOutputFunction",
            [state.expr, alg_state.expr, input.expr, param.expr, GRAVITY, head_to_platform_symbolic],  # type: ignore
            [output_expr],
            ["x", "z", "u", "p", "g", "T_HP"],
            ["y"],
        )

        # Head PVA Function
        self.headPva = cs.Function(
            "HeadPvaFunction",
            [state.expr, alg_state.expr, input.expr, param.expr, GRAVITY, head_to_platform_symbolic],  # type: ignore
            [head_pva_expr],
            ["x", "z", "u", "p", "g", "T_HP"],
            ["y"],
        )

        # Head Inertial Function
        self.headInertial = cs.Function(
            "HeadInertialFunction",
            [state.expr, alg_state.expr, input.expr, param.expr, GRAVITY, head_to_platform_symbolic],  # type: ignore
            [head_inertial_expr],
            ["x", "z", "u", "p", "g", "T_HP"],
            ["y"],
        )

        # Function to compute gravity vector in head frame.
        T_HW = cs.mtimes(platform_to_world, head_to_platform_symbolic)  # type: ignore
        self.headFrameGravity = cs.Function(
            "MotionPlatform_headFrameGravity",
            [state.expr, alg_state.expr, param.expr, GRAVITY, head_to_platform_symbolic],  # type: ignore
            [cs.mtimes(T_HW[:3, :3].T, GRAVITY)],  # type: ignore
            ["x", "z", "p", "g", "T_HP"],
            ["g"],
        )

    def evaluateNumericOutput(self, state=None, alg_state=None, input=None, param=None, head_to_platform=None, gravity=None):  # type: ignore
        """Evaluate output"""
        if state is None:
            state = self.state.nominal

        if alg_state is None:
            alg_state = self.algState.nominal

        if input is None:
            input = self.input.nominal

        if param is None:
            param = self.param.nominal

        if head_to_platform is None:
            head_to_platform = self.headToPlatform

        if gravity is None:
            gravity = self.gravity

        return self.output(x=state, z=alg_state, u=input, p=param, g=gravity, T_HP=head_to_platform)["y"]  # type: ignore

    def evaluateOutput(self, state=None, alg_state=None, input=None, param=None, head_to_platform=None, gravity=None):  # type: ignore
        """Evaluate output"""
        if state is None:
            state = self.state.expr

        if alg_state is None:
            alg_state = self.algState.expr

        if input is None:
            input = self.input.expr

        if param is None:
            param = self.param.expr

        if head_to_platform is None:
            head_to_platform = self.headToPlatform

        if gravity is None:
            gravity = self.gravity

        return self.output(x=state, z=alg_state, u=input, p=param, g=gravity, T_HP=head_to_platform)["y"]  # type: ignore

    def getOutputSymbolicStructure(self):
        return self.symbolicOutput

    def getReferenceOutputSymbolicStructure(self):
        return self.symbolicReferenceOutput

    def getOutputErrorSymbolicStructure(self):
        return self.symbolicOutput(self.symbolicOutput.cat - self.symbolicReferenceOutput.cat)  # type: ignore

    def getOutputNominal(self):
        return self.symbolicOutput(self.evaluateOutput(self.state.nominal, self.algState.nominal, self.input.nominal, self.param.nominal))  # type: ignore

    def getOutputSize(self) -> int:
        return self.symbolicOutput.size

    def getPlatformToWorldExpression(self) -> Union[cs.MX, T_NDArray]:
        return self.T_PW
