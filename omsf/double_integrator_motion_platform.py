##
## Copyright (c) 2015-2018 Mikhail Katliar, Max Planck Institute for Biological Cybernetics.
##
## This file is part of Offline Motion Simulation Framework (OMSF)
## (see https://github.com/mkatliar/omsf).
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Lesser General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Lesser General Public License for more details.
##
## You should have received a copy of the GNU Lesser General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##
from typing import Any, List, Tuple, Union

import numpy as np
from numpy import typing as npt

import casadi as cs  # type: ignore
import casadi_extras as ct
from omsf import typing

from .gravity import DEFAULT_GRAVITY, GRAVITY
from .motion_limits import MotionLimits
from .motion_platform import MotionPlatform
from .util import inertialSignalFromTransformationMatrix


class DoubleIntegratorMotionPlatform(MotionPlatform):

    def __init__(
        self,
        q: cs.MX,
        transformation_matrix: Union[cs.MX, typing.T_NDArray],
        motion_limits: List[MotionLimits],
        nominal_position: npt.NDArray[np.float64],
        param: ct.BoundedVariable = ct.BoundedVariable(),  # type: ignore
        v: Union[cs.MX, None] = None,
        u: Union[cs.MX, None] = None,
        constraint: "dict[str, Union[cs.MX, npt.NDArray[np.float64]]]" = {
            "expr": cs.MX.sym("constraint", 0),  # type: ignore
            "lb": -np.repeat(np.inf, 0),
            "ub": np.repeat(np.inf, 0),
        },
        gravity: npt.NDArray[np.float64] = DEFAULT_GRAVITY,
        head_to_platform: npt.NDArray[np.float64] = np.identity(4),
        **kwargs: "dict[str, Any]"
    ) -> None:
        """Constructor"""

        if v is None:
            v = cs.MX.sym("v", q.shape)  # type: ignore
        if u is None:
            u = cs.MX.sym("u", q.shape)  # type: ignore

        x: ct.struct_MX = ct.struct_MX([ct.entry("q", expr=q), ct.entry("v", expr=v)])  # type: ignore

        # Init DAE function
        x_dot: ct.struct_symMX = ct.struct_symMX(x)  # type: ignore
        dae: cs.MX = cs.vertcat(x_dot["q"] - v, x_dot["v"] - u)  # type: ignore

        # Create f_P, omega_P and alpha_P from T
        R_WP: cs.MX = cs.transpose(transformation_matrix[:3, :3])  # type: ignore
        assert isinstance(v, cs.MX) and isinstance(u, cs.MX)
        a_P, omega_P, alpha_P = inertialSignalFromTransformationMatrix(transformation_matrix, q, v, u)
        f_P: cs.MX = cs.mtimes(R_WP, GRAVITY) - a_P  # type: ignore
        transformation_matrix_platform_world = transformation_matrix

        # Axes limits
        q_min = np.array([l.positionMin for l in motion_limits])
        q_max = np.array([l.positionMax for l in motion_limits])
        v_min = np.array([l.velocityMin for l in motion_limits])
        v_max = np.array([l.velocityMax for l in motion_limits])
        u_min = np.array([l.accelerationMin for l in motion_limits])
        u_max = np.array([l.accelerationMax for l in motion_limits])
        # (q_min_finite,) = np.where(q_min > -np.Inf)
        # (q_max_finite,) = np.where(q_max < np.Inf)

        # Init expression for additional inequality constraints
        constr_ineq: ct.struct_MX = ct.struct_MX([ct.entry("cq_ineq", expr=constraint["expr"])])  # type: ignore
        lbg_ineq = constr_ineq()
        ubg_ineq = constr_ineq()
        lbg_ineq["cq_ineq"] = constraint["lb"]  # type: ignore
        ubg_ineq["cq_ineq"] = constraint["ub"]  # type: ignore

        # Combine stoppability and inequality constraints
        constr: ct.struct_MX = ct.struct_MX(
            [
                # ct.entry("cq_stop", expr = constr_stop.cat),
                ct.entry("cq_ineq", expr=constr_ineq.cat)  # type: ignore
            ]
        )
        lbg = constr()
        ubg = constr()
        # lbg["cq_stop"] = lbg_stop.cat
        lbg["cq_ineq"] = lbg_ineq.cat  # type: ignore
        # ubg["cq_stop"] = ubg_stop.cat
        ubg["cq_ineq"] = ubg_ineq.cat  # type: ignore

        # Init state bounds
        state_bounds = (x(), x())
        state_bounds[0]["q"] = q_min  # type: ignore
        state_bounds[1]["q"] = q_max  # type: ignore
        state_bounds[0]["v"] = v_min  # type: ignore
        state_bounds[1]["v"] = v_max  # type: ignore

        # Init base class
        # TODO: pass input and state bounds together as a dict similar to param and constr?

        assert isinstance(dae, cs.MX) and isinstance(f_P, cs.MX)

        MotionPlatform.__init__(
            self,
            state_derivative=x_dot,
            state=ct.Inequality(
                expr=x, lb=x({"q": q_min, "v": v_min}), ub=x({"q": q_max, "v": v_max}), nominal=x({"q": nominal_position, "v": 0})  # type: ignore
            ),
            input=ct.Inequality(expr=u, lb=u_min, ub=u_max, nominal=cs.DM.zeros(u.numel())),  # type: ignore
            dae=dae,
            f_P=f_P,
            omega_P=omega_P,
            alpha_P=alpha_P,
            transformation_matrix_platform_world=transformation_matrix_platform_world,
            constraint=ct.Inequality(expr=constr, lb=lbg, ub=ubg),  # type: ignore
            param=param,
            gravity=gravity,
            head_to_platform=head_to_platform,
        )

        self._motion_limits = motion_limits

    @property
    def numberOfAxes(self) -> int:
        return len(self._motion_limits)

    @staticmethod
    def _makeVariables(n_axes: int) -> Tuple[ct.struct_symMX, ct.struct_symMX, ct.struct_symMX]:
        x: ct.struct_symMX = ct.struct_symMX([ct.entry("q", shape=n_axes), ct.entry("v", shape=n_axes)])  # type: ignore
        z: ct.struct_symMX = ct.struct_symMX([ct.entry("empty", shape=0)])  # type: ignore
        u: ct.struct_symMX = ct.struct_symMX([ct.entry("u", shape=n_axes)])  # type: ignore

        return x, z, u
