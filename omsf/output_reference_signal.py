##
## Copyright Frank Drop and Mario Olivari.
##
## This file is part of Offline Motion Simulation Framework (OMSF) fork.
## (see https://gitlab.com/predictive-cueing/omsf-fork).
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Lesser General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Lesser General Public License for more details.
##
## You should have received a copy of the GNU Lesser General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##

from abc import ABC, abstractmethod

import numpy as np

import casadi as cs
import casadi_extras as ct
from omsf import util


def _interpolate(t, t_base, x):
    return util.interpolate(t, t_base, x)


class AbstractOutputReferenceSignal(ABC):
    """Defines interface for reference signals."""

    @property
    @abstractmethod
    def startTime(self):
        pass

    @property
    @abstractmethod
    def stopTime(self):
        pass

    @abstractmethod
    def sensorSignal(self, t):
        """Get reference signal for sensor output at time t."""
        pass

    @abstractmethod
    def platformSignal(self, t):
        """Get reference signal for platform output at time t."""
        pass


class OutputReferenceSignal(AbstractOutputReferenceSignal):
    """Defines platform and sensor output signals as continuous functions of time."""

    def __init__(self, t, y_platform_ref, y_sensor_ref=None) -> None:
        assert t.ndim == 1
        Nt = len(t)

        if y_sensor_ref is None:
            # TODO: Extend omsf to handle a NONE input for y_sensor_ref
            y_sensor_ref = np.array(
                [
                    [0],
                ]
                * Nt
            ).transpose()

        assert y_platform_ref.shape[1] == Nt
        assert y_sensor_ref.shape[1] == Nt

        self.y_platform_ref = y_platform_ref
        self.y_sensor_ref = y_sensor_ref
        self.time = t

    @property
    def startTime(self):
        return self.time[0]

    @property
    def stopTime(self):
        return self.time[-1]

    def sensorSignal(self, t):
        """Get reference signal for sensor output at time t."""
        return _interpolate(t, self.time, self.y_sensor_ref)

    def platformSignal(self, t):
        """Get reference signal for platform output at time t."""
        return _interpolate(t, self.time, self.y_platform_ref)

    def cut(self, t1, t2):
        """Cut sensory input between t1 and t2"""
        ind = np.logical_and(self.time >= t1, self.time <= t2)
        return OutputReferenceSignal(t=self.time[ind], y_platform_ref=self.y_platform_ref[:, ind], y_sensor_ref=self.y_sensor_ref[:, ind])
