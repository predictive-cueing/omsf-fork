import numpy
from numpy import typing as npt

T_NDArray = npt.NDArray[numpy.float64]
