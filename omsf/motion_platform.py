##
## Copyright (c) 2015-2018 Mikhail Katliar, Max Planck Institute for Biological Cybernetics.
##
## This file is part of Offline Motion Simulation Framework (OMSF)
## (see https://github.com/mkatliar/omsf).
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Lesser General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Lesser General Public License for more details.
##
## You should have received a copy of the GNU Lesser General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##
from typing import Union

import numpy as np

import casadi as cs  # type: ignore
import casadi_extras as ct
from omsf import transform, typing

from .gravity import DEFAULT_GRAVITY, GRAVITY
from .signal import INERTIAL_SIGNAL, INERTIAL_SIGNAL_ERROR, REFERENCE_INERTIAL_SIGNAL
from .typing import T_NDArray


class MotionPlatform(object):
    """Defines dynamics and output quantities of a motion platform.

    @param constr Inequality object defining platform constraints.
    """

    def __init__(
        self,
        state_derivative: ct.struct_symMX,
        state: ct.Inequality,
        input: ct.Inequality,
        dae: cs.MX,
        f_P: cs.MX,
        omega_P: cs.MX,
        alpha_P: cs.MX,
        transformation_matrix_platform_world: Union[cs.MX, typing.T_NDArray],
        alg_state: ct.BoundedVariable = ct.BoundedVariable(),  # type: ignore
        param: ct.BoundedVariable = ct.BoundedVariable(),  # type: ignore
        constraint: ct.Inequality = ct.Inequality(),  # type: ignore
        gravity: typing.T_NDArray = DEFAULT_GRAVITY,
        head_to_platform: typing.T_NDArray = np.identity(4),
    ):
        """Constructor"""

        self.stateDerivative = state_derivative
        self.state = state
        self.algState = alg_state
        self.input = input

        # Parameters
        self.param = param

        # Implicit DAE equations
        self.dae = dae

        # Expression for specific force in PF
        self.f_P = f_P

        # Expression for rotational velocity in PF
        self.omega_P = omega_P

        # Expression for rotational acceleration in PF
        self.alpha_P = alpha_P

        # Expression for PF->WF transformation matrix
        self.T_PW = transformation_matrix_platform_world

        # Constraints
        self.constraint = constraint

        # Gravity
        self.gravity = gravity

        # Head to platform transformation
        self.headToPlatform = head_to_platform

        # ------------------------------
        # Init platform output function.
        # ------------------------------
        # TODO: use transformInertialSignal()?

        # Recalculate to HF
        transformation_matrix_head_platform: cs.MX = cs.MX.sym("T_HP", 4, 4)  # type: ignore
        transformation_matrix_platform_head: cs.MX = transform.inv(transformation_matrix_head_platform)  # type: ignore
        rotation_matrix_head_platform: cs.MX = transformation_matrix_head_platform[:3, 3]  # type: ignore
        rotation_matrix_platform_head: cs.MX = transformation_matrix_platform_head[:3, :3]  # type: ignore

        f_H: cs.MX = cs.mtimes(  # type: ignore
            rotation_matrix_platform_head,
            f_P - cs.cross(alpha_P, rotation_matrix_head_platform) - cs.cross(omega_P, cs.cross(omega_P, rotation_matrix_head_platform)),  # type: ignore
        )
        omega_H: cs.MX = cs.mtimes(rotation_matrix_platform_head, omega_P)  # type: ignore
        alpha_H: cs.MX = cs.mtimes(rotation_matrix_platform_head, alpha_P)  # type: ignore

        y = ct.struct_MX(INERTIAL_SIGNAL)
        y["f"] = cs.densify(f_H)  # type: ignore
        y["omega"] = cs.densify(omega_H)  # type: ignore
        y["alpha"] = cs.densify(alpha_H)  # type: ignore

        self.output = cs.Function(
            "MotionPlatformOutputFunction",
            [state.expr, alg_state.expr, input.expr, param.expr, GRAVITY, transformation_matrix_head_platform],  # type: ignore
            [y],
            ["x", "z", "u", "p", "g", "T_HP"],
            ["y"],
        )

        # Function to compute gravity vector in head frame.
        transformation_matrix_head_world: cs.MX = cs.mtimes(transformation_matrix_platform_world, transformation_matrix_head_platform)  # type: ignore
        self.headFrameGravity = cs.Function(
            "MotionPlatform_headFrameGravity",
            [state.expr, alg_state.expr, param.expr, GRAVITY, transformation_matrix_head_platform],  # type: ignore
            [cs.mtimes(transformation_matrix_head_world[:3, :3].T, GRAVITY)],  # type: ignore
            ["x", "z", "p", "g", "T_HP"],
            ["g"],
        )

    def evaluateNumericOutput(self, state=None, alg_state=None, input=None, param=None, head_to_platform=None, gravity=None):  # type: ignore
        """Evaluate output"""
        if state is None:
            state = self.state.nominal

        if alg_state is None:
            alg_state = self.algState.nominal

        if input is None:
            input = self.input.nominal

        if param is None:
            param = self.param.nominal

        if head_to_platform is None:
            head_to_platform = self.headToPlatform

        if gravity is None:
            gravity = self.gravity

        return self.output(x=state, z=alg_state, u=input, p=param, g=gravity, T_HP=head_to_platform)["y"]  # type: ignore

    def evaluateOutput(self, state=None, alg_state=None, input=None, param=None, gravity=None, head_to_platform=None):  # type: ignore
        """Evaluate inertial signal"""

        if state is None:
            state = self.state.expr

        if alg_state is None:
            alg_state = self.algState.expr

        if input is None:
            input = self.input.expr

        if param is None:
            param = self.param.expr

        if head_to_platform is None:
            head_to_platform = self.headToPlatform

        if gravity is None:
            gravity = self.gravity

        return self.output(x=state, z=alg_state, u=input, p=param, g=gravity, T_HP=head_to_platform)["y"]  # type: ignore

    def getReferenceOutputSymbolicStructure(self):
        return REFERENCE_INERTIAL_SIGNAL

    def getOutputSymbolicStructure(self):
        return INERTIAL_SIGNAL

    def getOutputErrorSymbolicStructure(self):
        return INERTIAL_SIGNAL_ERROR

    def getOutputNominal(self):
        # TODO: evaluate with nominal signals?
        return INERTIAL_SIGNAL({"f": self.gravity, "omega": np.zeros(3), "alpha": np.zeros(3)})  # type: ignore

    def getOutputSize(self) -> int:
        return 9

    def getPlatformToWorldExpression(self) -> Union[cs.MX, T_NDArray]:
        return self.T_PW
